import sys
import boto3
from unittest  import mock
from moto import mock_dynamodb2

import src.boto3_example
from src.boto3_example import DynamodBExample
from botocore.exceptions import ClientError
from botocore.exceptions import ParamValidationError

model_instance = DynamodBExample()


@mock_dynamodb2
def test_create_dynamo_table():
    '''
        Implement the test logic here for testing create_dynamo_table method
    '''

    dynamodb = boto3.resource('dynamodb',region_name='us-east-1')
    tableName = model_instance.create_movies_table()

    assert tableName == 'Movies'


@mock_dynamodb2
@mock.patch('src.boto3_example.DynamodBExample.add_dynamo_record',return_value={'year': {'S':'2002'}, 'title':{'S':'Hello'}})
def test_add_dynamo_record_table(mock_output):
    '''
        Implement the test logic here for testing add_dynamo_record_table method
    '''

    dynamo_resource = boto3.resource('dynamodb', region_name='us-east-1')
    item = {'year': {'S':'2002'}, 'title':{'S':'Hello'}}
    expected = model_instance.add_dynamo_record('Movies', item)
    actual = item

    assert expected == actual

@mock_dynamodb2
@mock.patch('src.boto3_example.DynamodBExample.add_dynamo_record',return_value={'year': {'S':'2002'}, 'title':{'S':'Hello123'}})
def test_add_dynamo_record_table_failure(mock_output):
    '''
        Implement the test logic here test_add_dynamo_record_table method for failures
    '''
    dynamo_resource = boto3.resource('dynamodb', region_name='us-east-1')
    item = {'year': {'S': '2002'}, 'title': {'S': 'Hello'}}
    expected = model_instance.add_dynamo_record('Movies', item)
    actual = item

    assert expected != actual



