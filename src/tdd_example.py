class TDDExample():
    def __init__(self):
        pass

    def reverse_string(self, input_str:str) -> str:
        """
        Reverses order of characters in string input_str.
        """
        result = ""
        for i in input_str:
            result = i+result

        return result

    def find_longest_word(self, sentence: str) -> str :
        """
        Returns the longest word in string sentence.
        In case there are several, return the first.
        """

        l = list(sentence.split(" "))
        s = sorted(l,key=len)
        leng = len(s)
        return s[leng-1]

    def reverse_list(self, input_list: list) -> list:
        """
        Reverses order of elements in list input_list.
        """
        result = list()
        i= len(input_list)-1
        while(i>=0):
            result.append(input_list[i])
            i = i-1

        return result

    def count_digits(self, input_list: list, number_to_be_counted: int) -> int:
        """
        Return count of digits
        """
        occur = 0;
        occur = input_list.count(number_to_be_counted)
        return occur

